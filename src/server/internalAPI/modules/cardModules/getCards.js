
/**
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Object} nextaz
 * @returns {Object}
 */
const getAllCards = async (req, res) => {
  try {
     res.json({success: true, data: [
      {
        id: '1',
        name: 'John Brown',
        cardNo: 4111111111111111,
        balance: 2000,
        limit: 1000,
      },
      {
        id: '2',
        name: 'James Bonds',
        cardNo: 4111111111111112,
        balance: 3000,
        limit: 1000,
      },
      {
        id: '3',
        name: 'John Doe',
        cardNo: 4111111111111113,
        balance: 500,
        limit: 1000,        
      },
    ]});
  } 
  catch (error) {
  }
};

export default getAllCards;
