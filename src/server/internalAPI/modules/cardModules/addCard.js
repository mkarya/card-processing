
/**
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Object} nextaz
 * @returns {Object}
 */
const getAllCards = async (req, res) => {
  console.log(req.body);
  const { name, card, limit} = req.body;
  try {
     res.json({success: true, data: {
      id: Math.random()*10000,
      name,
      cardNo: card,
      balance: 0,
      limit,
    },});
  } 
  catch (error) {
  }
};

export default getAllCards;
