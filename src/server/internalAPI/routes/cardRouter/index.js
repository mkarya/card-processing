import express from 'express';
import getAllCards from '../../modules/cardModules/getCards';
import addCard from '../../modules/cardModules/addCard';

const router = express.Router();

router.post('/get-card-list', getAllCards);
router.post('/add-card', addCard);

export default router;