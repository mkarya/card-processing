import express from 'express';
import cardRauter from './cardRouter';

const router = express.Router();

router.use((req, res, next) => {
  next();
});

/**
 * Internal API
 */
router.use('/cards', cardRauter);

export default router;
