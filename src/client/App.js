import React from 'react';
import { Switch, Route } from 'react-router';
import Home from './routes/HomeComponent';

import store from "./redux";
import { Provider } from "react-redux";
import './App.less';
class App extends React.Component {
  render() {
    return <Provider store={store}>
      <Switch>
        <Route path="/" render={props => (<Home {...props} />)} />
      </Switch>
    </Provider>

  }
}
export default App;
