import types from './types';

/**
 * @param {Object} state - previous state
 * @param {Object} action - action to handle
 * @returns {Object} - new state
 */
const cardListReducer = (state = {}, action) => {
  switch (action.type) {
    case types.GET_CARD_LIST:
      return {
        ...state,
        cardList: action.payload,
      };
    case types.ADD_CARD:
      return {
        ...state,
        cardList: [...state.cardList, action.payload]
      };
    default:
      return state;
  }
};

export default cardListReducer;
