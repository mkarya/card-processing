import types from './types';
import fetchData from '../utils';
import constant from '../constant';

const { GET_CARD_LIST, ADD_CARD } = constant;

const getCards = () =>
  fetchData(
    types.GET_CARD_LIST,
    GET_CARD_LIST,
    '',
  );

const addCard = payload =>
  fetchData(
    types.ADD_CARD,
    ADD_CARD,
    payload,
  );

export default {
  getCards,
  addCard,
};
