export default {
  GET_CARD_LIST: '/api/cards/get-card-list',
  ADD_CARD: '/api/cards/add-card',
};