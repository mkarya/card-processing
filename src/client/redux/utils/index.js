import axios from 'axios';

const baseAction = (payload = {}, action) => ({
  type: action,
  payload,
});

const errorHandel = () => {
  return {
    type: "RECEIVE_ERROR"
  };
};


/**
 * Base function
 * @param {function} actionType type of action you want to dispatch
 * @param {string} apiURL URL OF API
 * @param {Object} data PAYLOAD
 * @returns {Promise}
 */
const fetchData = (actionType, apiURL, data) => async (
  dispatch,
) => {
  axios({
    method: 'post',
    url: apiURL,
    data,
    headers: {
      "Content-Type": "application/json"
    }
  }).then(res => res.data)
      .then(res => {
        if (!res.success) {
          throw new Error("No such user found!!");
        }
         dispatch(baseAction(res.data, actionType));
         return res;
      })
      .catch(err => dispatch(errorHandel()));
};

export default fetchData;
