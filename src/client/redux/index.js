import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import cardListReducer from './card_list';

export const rootReducer = combineReducers({
  [cardListReducer.name]: cardListReducer.reducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;

