import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cardActtion from '../../redux/card_list';

import Home from './CardList';
import './Cardlist.less';

const { getCards } = cardActtion.actions;

const mapStateToProps = state => ({
  cards: state[cardActtion.name].cardList,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getCards,
    },
    dispatch,
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
