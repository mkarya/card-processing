import React from 'react';
import { Table } from 'antd';

class CardList extends React.Component {

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  componentDidMount() {
    const { actions } = this.props;
    actions.getCards();
  }
  render() {
    const { cards = [] } = this.props;
    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Card No',
        dataIndex: 'cardNo',
        key: 'cardNo',
      },
      {
        title: 'Balance',
        dataIndex: 'balance',
        key: 'balance',
      },
      {
        title: 'Limit',
        dataIndex: 'limit',
        key: 'limit',
      },
    ];

    return !!cards.length && <Table rowKey={record => record.id}  columns={columns} dataSource={cards} className="card_list"/>
    
  }
}
export default CardList;
