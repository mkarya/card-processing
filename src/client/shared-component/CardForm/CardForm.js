import React from 'react';
import luhnCheck from '../../routes/utils';

class CardForm extends React.Component {

  constructor(props) {
    super(props);
    this.initialState = {
      name: '',
      card: '',
      limit: '',
    nameError: false,
    cardError: false,
    limitError: false,
  };
    this.state = this.initialState 
  }
  
handeler(target)  {
  const nametest = /^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$/;
  const amout = /^(0|[1-9]\d*)(\.\d+)?$/;
  switch(target.name) {
    case 'name': 
    this.setState({nameError: !nametest.test(target.value), [target.name]: target.value});
    default:
    this.setState({limitError: !amout.test(target.value), [target.name]: target.value});
  }
}

validate() {
  const { name, card, limit, nameError, cardError, limitError  } = this.state;
  const { actions } = this.props;

  if(!luhnCheck(card))  {
    return this.setState({cardError:true});
  }  else {
    this.setState({cardError: false})
  }

  if(name.length === 0){
    this.setState({nameError:true})
  }
  if(limit.length === 0){
    this.setState({limitError:true})
  }
  if((name && card && limit) && !(nameError  || cardError || limitError)) {
     actions.addCard({name, card, limit});
  } 

}
 render()  {
   const { nameError, cardError, limitError }  = this.state;
   return (
    <div className="card_form">
    <form>
    <label>Name <input type="text" name="name" onChange={e => this.handeler(e.target)} className={nameError ? 'error': ''}></input></label>
    <label>Card <input type="number" name="card" onChange={e => this.handeler(e.target)} className={cardError ? 'error': ''}></input></label>
    <label>Limit <input type="text" name="limit" onChange={e => this.handeler(e.target)} className={limitError ? 'error': ''}></input></label>
    <button  type="button" onClick={() => this.validate()} >ADD CARD</button>
    </form>
    </div >
   )
 }
}

export default CardForm;
