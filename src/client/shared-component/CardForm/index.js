import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cardActtion from '../../redux/card_list';

import Home from './CardForm';
import './cardform.less';

const { addCard, getCards } = cardActtion.actions;

const mapStateToProps = state => ({
  cards: state[cardActtion.name].cardList,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      addCard,
      getCards
    },
    dispatch,
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
