import React from 'react';
import CardList from '../../shared-component/CardList';
import CardForm from '../../shared-component/CardForm';
import './Home.less';

class Home extends React.Component {
  render() {
    return <div className="container">
      <h1>Credit Card System</h1>
      <CardForm />
      <CardList />
      
    </div>
  }
}
export default Home;
