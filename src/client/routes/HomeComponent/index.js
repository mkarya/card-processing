import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cardActtion from '../../redux/card_list';

import Home from './Home';


const { getCards, addCard } = cardActtion.actions;

const mapStateToProps = state => ({
  cards: state[cardActtion.name].cardList,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      getCards, addCard,
    },
    dispatch,
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
