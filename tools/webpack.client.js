const path = require('path');

module.exports = {
  target: 'node',
  mode: 'development',
  entry: {
    app: ['@babel/polyfill', path.join(__dirname, 'client.js')]
  },
  output: {
    filename: 'client.js',
    path: path.resolve(__dirname, '../build/public'),
    publicPath: '/build/public'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: require.resolve('babel-loader'),
        // exclude: ,
        options: {
          presets: [
              require.resolve('@babel/preset-env'),
              require.resolve('@babel/preset-react'),
              require.resolve('@babel/preset-flow')
          ],
          plugins: [
            [
              require.resolve('babel-plugin-import'),
              {
                libraryName: 'antd',
                style: 'css',
              },
            ],
            require.resolve('@babel/plugin-proposal-class-properties'),
            require.resolve('@babel/plugin-syntax-dynamic-import'),
          ],
      }
      },
      { 
                test: /\.(css|less)$/,
                use: [ 
                    'style-loader',
                    'css-loader', 
                    'less-loader'
                ],
            },
    ]
  },
}
