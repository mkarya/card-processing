import express from 'express';
import bodyParser from  'body-parser';
import cookieParser from 'cookie-parser';

import internalApi from '../src/server/internalAPI/routes'
import html from './html';

const { env } = process;

const port = env.PORT || 4000;
const app = express();

app.use(cookieParser());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

app.use(express.static('build/public'));

app.get('*', (req, res, next) => {
  const finalContent = html(req);
   res.send(finalContent);
});

app.use('/api', internalApi);

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});