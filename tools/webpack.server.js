const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');
module.exports = {
  target: 'node',
  mode: 'development',
  entry: {
    app: ['@babel/polyfill', path.join(__dirname, 'server.js')]
  },
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, '../build'),
    publicPath: '/build'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: require.resolve('babel-loader'),
        // exclude: ,
        options: {
          presets: [
            require.resolve('@babel/preset-env'),
            require.resolve('@babel/preset-react'),
            require.resolve('@babel/preset-flow')
          ],
        }
      },
      {
        test: /\.(css|less)$/,
        use: [
          { loader: 'style-loader', options: { injectType: 'lazyStyleTag' } },
          'css-loader',
          { loader: 'less-loader', options: { injectType: 'lazyStyleTag' } },
          // 'less-loader'
        ],
      },
    ]
  },
  externals: [webpackNodeExternals()]
}

// "presets": ["@babel/preset-env", '@babel/preset-flow', '@babel/preset-react']