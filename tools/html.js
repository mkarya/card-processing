import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router';
import App from '../src/client/App';



const html = req => {
  const context = {};
  const render = ReactDOMServer.renderToString(
    <StaticRouter location={req.url} context={context}>
      <App />
    </StaticRouter>);

  return `<html>
  <head>
    <title>Card Processing</title>
  </head>
  <body>
    <div id='card_root'>${render}</div>
  </body>
  <script src="client.js"></script>
</html>`};

export default html;