## Card Processing system (Server-side rendering)

### Requirements

- Mac OS X, Windows, or Linux
- [Yarn](https://yarnpkg.com/) package + [Node.js](https://nodejs.org/)  newer
- Text editor or IDE pre-configured with React/JSX/Flow/ESlint

### Quick Start (Instructions for running on your local machine)

1.  Clone the repo
2.  Install yarn modules

```shell
yarn install
```
Also, see yarn installation instructions: https://yarnpkg.com/lang/en/docs/install/

3.  Run application

```shell
yarn run dev
```

# How application works

Both server/client parts processes by webpack. So you're able to use all webpack plugins and new features while development.

It would create and fullfill `build` directory. With `server.js` inside (entire point of application), and `public` directory with client code and assets.

## How it works

When you start `server.js` (`build/server.js` in dev mode) - it reads `server.js` with express-application. And starts it on the port, defined in `env.PORT` 

**Note:** **_that port would be applied only after build. During development port would be `:4000` and changing port in config wouldn't affect an application in dev-mode._**


# Root directory layout

Before you start, take a moment to see how the project structure looks like:

```
.
├── /build/                     # The folder for compiled output
├── /node_modules/              # 3rd-party libraries and utilities
├── /src/                       # The source code of the application
│   ├── /client/                # Client side code
│   ├── /server/                # Express server code
├── /tools/                     # Build automation scripts and utilities
│   ├── /client.js              
│   ├── /server.js              
│   ├── /html.js               
│   └── /webpack.client.js      # Configurations for client-side and server-side bundles
│   └── /webpack.server.js      # Configurations for server-side and server-side bundles
├── package.json                # The list of 3rd party libraries and utilities
└── yarn.lock                   # Fixed versions of all the dependencies
```
